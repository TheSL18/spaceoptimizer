# Spaceoptimizer


SpaceOptimizer ha sido creado bajo la necesidad de automatizar tareas complejas de limpieza de forma segura sin tener que ir navegando por los directorios

Se inicio con la versión 1.0.0 que no hacia nada mas que automatizar todo el proceso sin que el usuario interviniera mucho

La versión 1.0.1 introdujo cambios para que el usuario decidiera que cambios específicos quería efectuar es decir introdujo un menú donde el usuario puede seleccionar la acción que desea realizar independientemente de realizar todo el proceso de un solo golpe

la versión 1.0.2 introduce un actualizador que descarga del repositorio de GitLab las nuevas actualizaciones del Script para que siempre se mantenga al día en funcionalidad



Forma de uso.

dependiendo de donde se haya realizado la instalación de la herramienta el usuario deberá escribir en la terminal las siguientes opciones

si descargo la herramienta por primera vez desde GtiLab en la carpeta raíz los comandos a utilizar serán

"#./SpaceOptimizer.sh"
bash SpaceOptimizer.sh
./SpaceOptimizer.sh

de aquí en adelante el menú de usuario es muy intuitivo y simple para su comprensión

[+] Presione 1 para borrar boot.log
[+] Presione 2 para borrar messages
[+] Presione 3 para mover el archivo parásito a /opt/remote
[+] Presione 4 para descaragar nuevas veriones del repositorio

la opción numero 1 borrara el archivo boot.log que almacena los registros de los inicios del sistema

la opción numero 2 borrara el archivo messages del sistema donde se almacenan los mensajes de error o mensajes de aplicaciones

la opción numero 3 fue implementada unicamente para el sensor de mi banco ya que el crea un archivo que es el registro de una ip específicamente el archivo 192.168.50.1.log que es el causante de que el sensor este siempre al limite de almacenamiento lo que hace es buscar si existe el archivo y en caso de existir permite ver los demás nombres de archivo existentes y moverlos a un directorio especial para almacenarlos

la opcion 4 realizara la actualización del script automatizando el proceso de descarga, movimientos de la nueva actualización y eliminar los archivos residuales después de terminar el proceso

a medida que se necesiten mas acciones se irán agregando para que lleguen en las próximas actualizaciones
el codigo se puede auditar desde GitLab descargando la ultima fuente para su transparencia y que no incluya codigo maligno
