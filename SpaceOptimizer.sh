#!/bin/bash
#kevin david muñoz moreno
#tel 3025685220
#email david.munozm@protonmail.com

PROGNAME=SpaceOptimizer
NAMEPROPER=SpaceOptimizer
VERSION=1.1.1

if [ "$EUID" -ne 0 ]; then
  echo "Debe ser el usuario 'root' para utilizar esta utilidad"
  exit
fi

function ctrl_c() {
  echo -e "\n[!] Saliendo...\n"
  exit 1
}

#Ctrl+C
trap ctrl_c INT

clear
echo "░██████╗██████╗░░█████╗░░█████╗░███████╗░█████╗░██████╗░████████╗██╗███╗░░░███╗██╗███████╗███████╗██████╗░"
echo "╚█████╗░██████╔╝███████║██║░░╚═╝█████╗░░██║░░██║██████╔╝░░░██║░░░██║██╔████╔██║██║░░███╔═╝█████╗░░██████╔╝"
echo "░╚═══██╗██╔═══╝░██╔══██║██║░░██╗██╔══╝░░██║░░██║██╔═══╝░░░░██║░░░██║██║╚██╔╝██║██║██╔══╝░░██╔══╝░░██╔══██╗"
echo "██████╔╝██║░░░░░██║░░██║╚█████╔╝███████╗╚█████╔╝██║░░░░░░░░██║░░░██║██║░╚═╝░██║██║███████╗███████╗██║░░██║"
echo "╚═════╝░╚═╝░░░░░╚═╝░░╚═╝░╚════╝░╚══════╝░╚════╝░╚═╝░░░░░░░░╚═╝░░░╚═╝╚═╝░░░░░╚═╝╚═╝╚══════╝╚══════╝╚═╝░░╚═╝"

echo "█▄▄ █▄█   █▀▄▀█ █▀█ ░   █░█ ▄▀█ █▀▀ █▄▀ █▀▀ █▀█"
echo "█▄█ ░█░   █░▀░█ █▀▄ ▄   █▀█ █▀█ █▄▄ █░█ ██▄ █▀▄"

echo ""

echo "[+] Script para automatizar la tareas de los sensores splunk"
echo "[+] Por favor escoge una opcion y pulsa enter para continuar o pulsa Ctrl-C para salir:"
echo "-------------------------------------------------------------------------------------------"

echo "[+] Presione 1 para borrar boot.log "
echo "[+] Presione 2 para borrar messages "
echo "[+] Presione 3 para realizar el metodo 1 mover el archivo "192.168.50.1" desbordado a /opt/remote (Solo sensor Mi Banco) "
echo "[+] Presione 4 para realizar el metodo 2 comprimir el archivo "192.168.50.1" desbordado en /var/log/remote/ (Solo sensor Mi Banco) "
echo "[+] Presione 5 para descaragar nuevas veriones del repositorio "
echo "[+] Presione 6 para crear la sincronizacion de hora con servidores seguros"
echo "[+] Presione 7 para actualizar repositorios"
echo "[+] Preciona 8 para solucionar los errores de Search LAG en los sensores que lo tengan"
echo "[+] Presione 99 para ver la version del Script "

echo "[+] Ingresa una opción: "
# Lee la variable ingresada para acceder al menú
read opcion

# Usa case para encontrar un match en el menu de opciones y proceder con la accion
case $opcion in

# Opción 1
1)
  echo "[+] Borrando boot.log"
  echo "-------------------------------------------------------------------------------------------"
  echo "[-] boot.log"
  echo "[!]"
  rm -v /var/log/boot.log
  echo "[+] completo"
  sleep 2
  bash SpaceOptimizer.sh
  ;;

# Opción 2
2)
  echo "[-] Borrando messages"
  echo "-------------------------------------------------------------------------------------------"
  echo "[!]"
  rm -v /var/log/messages
  echo "[+] completo"
  sleep 2
  bash SpaceOptimizer.sh
  ;;

# Opción 3
3)
  hostname=$1
  if [ "searchBChead" != "$hostname" ]; then
    echo "No estas en el sensor de mi banco abortando accion"
    exit
  fi
  echo "[+] Metodo 1 mover archivo desbordado 192.168.50.1"
  echo "-------------------------------------------------------------------------------------------"
  echo "[+] asigna un nuevo nombre al archivo"
  echo "-------------------------------------------------------------------------------------------"
  #buscar archivos existentes
  nombren=$(ls /opt/remote)

  #mostrar nombre
  echo "[+] existen estos nombres:
$nombren"
  echo "-------------------------------------------------------------------------------------------"

  echo "[+] Ingresa el nuevo nombre: "
  echo "-------------------------------------------------------------------------------------------"

  #asignar nombre
  echo "[+] asignando $nuevonombre"
  echo "[!]"
  mv -v /var/log/remote/192.168.50.1/192.168.50.1.log /opt/remote/$nuevonombre
  echo "[+] completo"
  sleep 2
  bash SpaceOptimizer.sh
  ;;

# Opción 4
4)
 hostname=$1
  if [ "searchBChead" != "$hostname" ]; then
    echo "No estas en el sensor de mi banco abortando accion"
    exit
  fi
echo "[+] Metodo 2 comprimir archivo desbordado 192.168.50.1"
logrotate -v /var/log/remote/logrotate.conf
 echo "[+] completo"
 sleep 2
 bash SpaceOptimizer.sh
 ;;

# Opción 5
5)
  echo "[+] Descargando nueva version del repositorio"
  echo "-------------------------------------------------------------------------------------------"

  # descargando actualizaciones del repositorio
  git clone https://gitlab.com/TheSL18/spaceoptimizer.git
  cd spaceoptimizer
  sudo chmod +x SpaceOptimizer.sh
  sudo cp SpaceOptimizer.sh /bin
  cd ..
  sleep 2
  echo "[-] Eliminando ficheros de instalación para ahorrar espacio en el disco "
  sleep 3
  sudo rm -v -r spaceoptimizer/
  sleep 4
  echo "[+] Instalado satisfactoriamente!!"
  sleep 2
  bash SpaceOptimizer.sh
  ;;

# Opción 6
6)
  echo "[+] Creando Script para sincronizacion de hora"
  echo "-------------------------------------------------------------------------------------------"
  sudo dnf install chrony -y
  sudo rm /etc/chrony.conf
  sudo echo "IyBVc2UgcHVibGljIHNlcnZlcnMgZnJvbSB0aGUgcG9vbC5udHAub3JnIHByb2plY3QuCiMgUGxl
YXNlIGNvbnNpZGVyIGpvaW5pbmcgdGhlIHBvb2wgKGh0dHA6Ly93d3cucG9vbC5udHAub3JnL2pv
aW4uaHRtbCkuCnNlcnZlciAzLmNvLnBvb2wubnRwLm9yZwpzZXJ2ZXIgMy5zb3V0aC1hbWVyaWNh
LnBvb2wubnRwLm9yZwpzZXJ2ZXIgMC5zb3V0aC1hbWVyaWNhLnBvb2wubnRwLm9yZwoKIyBSZWNv
cmQgdGhlIHJhdGUgYXQgd2hpY2ggdGhlIHN5c3RlbSBjbG9jayBnYWlucy9sb3NzZXMgdGltZS4K
ZHJpZnRmaWxlIC92YXIvbGliL2Nocm9ueS9kcmlmdAoKIyBBbGxvdyB0aGUgc3lzdGVtIGNsb2Nr
IHRvIGJlIHN0ZXBwZWQgaW4gdGhlIGZpcnN0IHRocmVlIHVwZGF0ZXMKIyBpZiBpdHMgb2Zmc2V0
IGlzIGxhcmdlciB0aGFuIDEgc2Vjb25kLgptYWtlc3RlcCAxLjAgMwoKIyBFbmFibGUga2VybmVs
IHN5bmNocm9uaXphdGlvbiBvZiB0aGUgcmVhbC10aW1lIGNsb2NrIChSVEMpLgpydGNzeW5jCgoj
IEVuYWJsZSBoYXJkd2FyZSB0aW1lc3RhbXBpbmcgb24gYWxsIGludGVyZmFjZXMgdGhhdCBzdXBw
b3J0IGl0LgojaHd0aW1lc3RhbXAgKgoKIyBJbmNyZWFzZSB0aGUgbWluaW11bSBudW1iZXIgb2Yg
c2VsZWN0YWJsZSBzb3VyY2VzIHJlcXVpcmVkIHRvIGFkanVzdAojIHRoZSBzeXN0ZW0gY2xvY2su
CiNtaW5zb3VyY2VzIDIKCiMgQWxsb3cgTlRQIGNsaWVudCBhY2Nlc3MgZnJvbSBsb2NhbCBuZXR3
b3JrLgojYWxsb3cgMTkyLjE2OC4wLjAvMTYKCiMgU2VydmUgdGltZSBldmVuIGlmIG5vdCBzeW5j
aHJvbml6ZWQgdG8gYSB0aW1lIHNvdXJjZS4KI2xvY2FsIHN0cmF0dW0gMTAKCiMgU3BlY2lmeSBm
aWxlIGNvbnRhaW5pbmcga2V5cyBmb3IgTlRQIGF1dGhlbnRpY2F0aW9uLgprZXlmaWxlIC9ldGMv
Y2hyb255LmtleXMKCiMgR2V0IFRBSS1VVEMgb2Zmc2V0IGFuZCBsZWFwIHNlY29uZHMgZnJvbSB0
aGUgc3lzdGVtIHR6IGRhdGFiYXNlLgpsZWFwc2VjdHogcmlnaHQvVVRDCgojIFNwZWNpZnkgZGly
ZWN0b3J5IGZvciBsb2cgZmlsZXMuCmxvZ2RpciAvdmFyL2xvZy9jaHJvbnkKCiMgU2VsZWN0IHdo
aWNoIGluZm9ybWF0aW9uIGlzIGxvZ2dlZC4KI2xvZyBtZWFzdXJlbWVudHMgc3RhdGlzdGljcyB0
cmFja2luZwo=" | base64 -d >/etc/chrony.conf
  sudo systemctl enable --now chronyd
  sudo timedatectl set-timezone America/Bogota
  sudo timedatectl set-ntp true
  sudo systemctl restart chronyd
  sleep 3
  echo "[+] completo"
  sleep 3
  bash SpaceOptimizer.sh
  ;;

# Opción 7
7)
  echo "[+] actualizando repositorios"
  echo "-------------------------------------------------------------------------------------------"
  sudo dnf install --nogpgcheck https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm -y
  sudo dnf install --nogpgcheck https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-8.noarch.rpm https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-8.noarch.rpm -y
  sudo yum install -y https://rpms.remirepo.net/enterprise/remi-release-8.rpm
  sudo dnf config-manager --enable powertools
  sudo subscription-manager repos --enable "codeready-builder-for-rhel-8-$(uname -m)-rpms"
  sudo yum update -y
  sleep 3
  echo "[+] Completo"
  sleep 3
  bash SpaceOptimizer.sh
  ;;

# Opción 8
8)
  echo "[+] Reiniciando sensores"
  echo "-------------------------------------------------------------------------------------------"
  sudo /opt/splunk/bin/splunk restart
  sleep 3
  echo "[+] Completo"
  sleep 3
  bash SpaceOptimizer.sh
  ;;

# Opción 99
99)
  echo "[+] Version..."
  echo "-------------------------------------------------------------------------------------------"
  echo "[+] 1.0.3"
  echo "-------------------------------------------------------------------------------------------"
  sleep 3
  bash SpaceOptimizer.sh
  ;;

esac
